var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var numClients = 0;

app.get('/', (req, res) => {
	res.sendFile(__dirname+'/index.html');
});

var ioroom = io.of('/chatroom');
ioroom.on('connection', (socket) => {
	console.log('User konek!');
	socket.emit('pesan', {msg: 'Aku sayang lina'});
});

app.get('/chatroom', (req, res) =>{
	res.sendFile(__dirname+'/clientroom.html');
});

io.on('connection', (socket) => {
	//server to client
	//socket.emit('pengumuman', {msg: 'Hello user baru!'});

	//client to server
	//socket.on('kirim', (data) => {
	//	console.log('pesan: ', data.msg);
	//});

	//counting user
	numClients++;
    io.emit('statsuser', {jmlUser: numClients });

    console.log('Connected clients:', numClients);

    socket.on('disconnect', () => {
        numClients--;
        io.emit('statsuser', {jmlUser: numClients });

        console.log('Connected clients:', numClients);
    });
});

server.listen(6969);
